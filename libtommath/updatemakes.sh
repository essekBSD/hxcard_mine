#!/bin/bash

./helper.pl --update-makefiles || exit 1

makefiles=(makefile makefile.shared makefile_include.mk makefile.msvc makefile.unix makefile.mingw)
vcproj=(libtommath_VS2008.vcproj)

if [ $# -eq 1 ] && [ "$1" == "-c" ]; then
  git add ${makefiles[@]} ${vcproj[@]} && git commit -m 'Update makefiles'
fi

exit 0

# ref:         HEAD -> develop
# git commit:  62adf47fb8b96fa11def1291a20126598a5dc110
# commit time: 2018-12-31 11:53:25 +0100
